//// These tests are for UNIX systems only

import gleam/should
import gleam/path.{Absolute, Relative}

pub fn path_kind_test() {
  path.kind("foo/bar")
  |> should.equal(Relative)

  path.kind("/foolbar")
  |> should.equal(Absolute)
}
