import gleam/file
import gleam/posix_errors.{Eexist, Eisdir}
import gleam/path
import gleam/should

pub fn directory_creation_deletion_test() {
  let dir_path = "./xxxxone/two/three/"
  let file_path = "./xxxxone/two/three/test.txt"

  file.ensure_directory(dir_path)
  |> should.equal(Ok(Nil))

  file.write_string(dir_path, "hello test 😀")
  |> should.equal(Error(Eisdir))

  file.write_string(file_path, "hello test 😀")
  |> should.equal(Ok(Nil))

  file.exists(file_path)
  |> should.equal(True)

  file.is_file(file_path)
  |> should.equal(True)

  file.is_directory(file_path)
  |> should.equal(False)

  file.read_to_bitstring(file_path)
  |> should.equal(Ok(<<"hello test 😀":utf8>>))

  file.delete_empty_directory(dir_path)
  |> should.equal(Error(Eexist))

  file.delete(file_path)
  |> should.equal(Ok(Nil))

  file.delete_empty_directory(dir_path)
  |> should.equal(Ok(Nil))

  file.delete_non_empty_directory("./xxxxone/")
  |> should.equal(Ok(Nil))
}

pub fn cwd_cd_test() {
  let dir_path = "./xxxxtest/"
  assert Ok(here) = file.cwd()
  let _ = file.ensure_directory(dir_path)

  let _ = file.cd(dir_path)

  file.cwd()
  |> should.not_equal(Ok(here))

  let _ = file.cd(here)

  file.cwd()
  |> should.equal(Ok(here))

  file.delete_non_empty_directory(dir_path)
}
